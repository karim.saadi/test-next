export class TodosAPI {
    /**
     * 
     * @returns 
     */
    async getAllTodos(): Promise<any[]> {
        return fetch('https://jsonplaceholder.typicode.com/todos')
            .then(response => response.json())
    }
    /**
     * 
     * @returns 
     */
    async getAllTodosId(): Promise<any[]> {
        return this.getAllTodos()
            .then(todos => todos.map(todo => ({
                params: { id: String(todo.id) }
            })))
    }

    /** */
    async getOneTodo(todoId: number): Promise<any> {
        return this.getAllTodos()
            .then(todos => todos.find(todo => todo.id === todoId))
    }
}