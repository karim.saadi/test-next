import { GetStaticPaths } from "next";
import { TodosAPI } from "../../lib/api/todos.api";

export default function Post({ todo }) {
  return (
    <main>
      <article>
        <h1>Todo numéro: {todo.id} </h1>
        <p>{todo.title}</p>
      </article>
    </main>

  );
}

export const getStaticPaths: GetStaticPaths = async () => {
  const paths: any[] = await new TodosAPI().getAllTodosId();
  return {
    paths,
    fallback: false,
  };
}

export const getStaticProps = async ({ params }) => {
  const todo: any = await new TodosAPI().getOneTodo(Number(params.id))
  return {
    props: {
      todo,
    },
  };
}