import { GetStaticProps } from 'next';
import Link from 'next/link';
import { TodosAPI } from '../lib/api/todos.api';

export default function Home({ allPostsData }) {
  return (
    <main>
      <h1>My Todos</h1>
      {
        allPostsData.map(post => (
          <article key={post.id}>
            <Link href={`todos/${post.id}`}>{post.title}</Link>
          </article>
        ))
      }
    </main>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  try {
    const allPostsData = await new TodosAPI().getAllTodos();
    return {
      props: {
        allPostsData,
      },
    };
  } catch (error) {
    return {
      props: null,
      notFound: 404,
    }
  }
}


